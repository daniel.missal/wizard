import os.path
from math import exp, sqrt
from data import *
from game import *



class Crated_player:

  def __init__(self, name, type, rating, no_of_games, aggregated_points, max_points, min_points, avg_points, var_points, points, won_games):

    self.name              = name
    self.type              = type
    self.rating            = rating
    self.no_of_games       = no_of_games
    self.aggregated_points = aggregated_points
    self.max_points        = max_points
    self.min_points        = min_points
    self.avg_points        = avg_points
    self.var_points        = var_points
    self.points            = points
    self.won_games         = won_games


def get_rating_list_from_file():

  rating_list = {}

  if os.path.exists("rating_list.txt"):

    file = open("rating_list.txt", "r")

    content = [line.strip() for line in file.readlines()]

    line_index = 0

    while (line_index + 9 < len(content)):

      name              = content[line_index]
      type              = content[line_index + 1]
      rating            = content[line_index + 2]
      no_of_games       = content[line_index + 3]
      aggregated_points = content[line_index + 4]
      max_points        = content[line_index + 5]
      min_points        = content[line_index + 6]
      avg_points        = content[line_index + 7]
      var_points        = content[line_index + 8]
      won_games         = content[line_index + 9]

      rated_player = Crated_player(name, type, int(rating), int(no_of_games), int(aggregated_points), int(max_points), int(min_points), float(avg_points),
                                   float(var_points), [], float(won_games))

      rating_list[name] = rated_player

      line_index += 10

    file.close()

  else:

    open("rating_list.txt", "w").close()                                                # Create an empty file

  return rating_list


def add_player(player, rating_list):                                                    # This function creates a new entry in a rating list object.

  rated_player = Crated_player(player.name, player.type, 1500, 0, 0, -9999, 9999, 0, 0, [], 0)

  rating_list[player.name] = rated_player

  return rated_player


def evaluate_game(game):

  no_of_players = game.no_of_players
  players       = game.players
  final_points  = game.final_points

  #print("game.id: %i" % game.id)
  #for p in range(no_of_players):
    #print("%-30s %i" % (players[p].name, final_points[p]))

  rating_gain = [0 for player_index in range(no_of_players)]                         # At the end the players will gain these points for this game.
                                                                                        # Storing gained points in this list temporarily and adding them only
                                                                                        # after all pairs are evaluated ensures the order-indepdendence of the pair evaluations.

  for index1 in range(no_of_players - 1):                                            # All pairs of players in this game are considered independently
    for index2 in range(index1 + 1, no_of_players):                                     #

      points1 = final_points[index1]
      points2 = final_points[index2]

      rating1 = players[index1].rating_list_entry.rating
      rating2 = players[index2].rating_list_entry.rating

      if points1 > points2:                                                                             # If the first player of the pair got more points than the second...

        gain = int(100 / (1 + exp(-0.007 * (rating2 - rating1 - 300))) * (points1 - points2) / 200)     # Using a logistic function with steepness = 0.007,
                                                                                                        # maximum = 100, x-value of midpoint = 300
                                                                                                        # and multiplicating the result with (points1 - points2) / 200

        rating_gain[index1] += gain                                                                             # Conservation of points is ensured this way
        rating_gain[index2] -= gain

      elif points1 == points2:                                                                          # If both players of the pair got an equal number of points...

        if rating1 > rating2:

          gain = int(0.1 * (rating2 - rating1))

          rating_gain[index1] += gain
          rating_gain[index2] -= gain

        if rating1 < rating2:

          gain = int(0.1 * (rating1 - rating2))

          rating_gain[index1] -= gain
          rating_gain[index2] += gain

      else:                                                                                             # If the second player of the pair got more points than the first...

        gain = int(100 / (1 + exp(-0.007 * (rating1 - rating2 - 300))) * (points2 - points1) / 200)

        rating_gain[index1] -= gain
        rating_gain[index2] += gain

  for player_index in range(no_of_players):

    rating_list_entry = players[player_index].rating_list_entry

    rating_list_entry.rating            += rating_gain[player_index]
    rating_list_entry.no_of_games       += 1

    points = final_points[player_index]

    rating_list_entry.aggregated_points += points

    if points > rating_list_entry.max_points:
      rating_list_entry.max_points = points

    if points < rating_list_entry.min_points:
      rating_list_entry.min_points = points

    rating_list_entry.avg_points        = float(rating_list_entry.aggregated_points) / rating_list_entry.no_of_games

    rating_list_entry.points.append(points)

    if points == max(final_points):
      rating_list_entry.won_games += 1 / final_points.count(points)


def save_rating_list_to_file(rating_list):

  file = open("rating_list.txt", "w")

  for name, rated_player in rating_list.items():

    #print("Entry name = '%s'" % name)
    #print(" rated_player.name = '%s'" % rated_player.name)
    file.write(rated_player.name + "\n")
    #print(" rated_player.type = '%s'" % rated_player.type)
    file.write(" " + rated_player.type + "\n")
    #print(" rated_player.rating = '%s'" % rated_player.rating)
    file.write(" " + str(rated_player.rating) + "\n")
    #print(" rated_player.no_of_games = '%s'" % rated_player.no_of_games)
    file.write(" " + str(rated_player.no_of_games) + "\n")
    #print(" rated_player.aggregated_points = '%s'" % rated_player.aggregated_points)
    file.write(" " + str(rated_player.aggregated_points) + "\n")
    #print(" rated_player.max_points = '%s'" % rated_player.max_points)
    file.write(" " + str(rated_player.max_points) + "\n")
    #print(" rated_player.min_points = '%s'" % rated_player.min_points)
    file.write(" " + str(rated_player.min_points) + "\n")
    #print(" rated_player.avg_points = '%s'" % rated_player.avg_points)
    file.write(" " + str(rated_player.avg_points) + "\n")
    #print(" rated_player.avg_points = '%s'" % rated_player.var_points)
    rated_player.var_points = sqrt(sum([(i - rated_player.avg_points)**2 for i in rated_player.points]) / rated_player.no_of_games)
    file.write(" " + str(rated_player.var_points) + "\n")
    file.write(" " + str(rated_player.won_games) + "\n")

  file.close()


  file = open("rating_list_sorted_by_rating.txt", "w")

  file.write("Name                           Rating   Type   Games   Won games   Sum of points   Max. points   Min. points  Std. points   Avg. points\n")
  file.write("=======================================================================================================================================\n")

  sorted_rating_list = sorted(list(rating_list.values()), key=lambda Crated_player : Crated_player.rating, reverse = True)

  for player in sorted_rating_list:
    file.write("%-30s   %4i      %s  %6i    %6.3f %%   %13i         %5i         %5i     %8.3f      %8.3f\n" \
               % (player.name, player.rating, player.type, player.no_of_games, player.won_games / player.no_of_games * 100,
                  player.aggregated_points, player.max_points, player.min_points, player.var_points, player.avg_points))

  file.close()


  file = open("rating_list_sorted_by_avg_points.txt", "w")

  file.write("Name                           Rating   Type   Games   Won games   Sum of points   Max. points   Min. points  Std. points   Avg. points\n")
  file.write("=======================================================================================================================================\n")

  sorted_rating_list = sorted(list(rating_list.values()), key=lambda Crated_player : Crated_player.avg_points, reverse = True)

  for player in sorted_rating_list:
    file.write("%-30s   %4i      %s  %6i    %6.3f %%   %13i         %5i         %5i     %8.3f      %8.3f\n" \
               % (player.name, player.rating, player.type, player.no_of_games, player.won_games / player.no_of_games * 100,
                  player.aggregated_points, player.max_points, player.min_points, player.var_points, player.avg_points))

  file.close()


def evaluate_list(games):

  # This functions receives a list of games and evaluates all of them while new players are registered if necessary.

  rating_list = get_rating_list_from_file()

  file = open("game_results.txt","a")

  for game in games:

    file.write("--- Game of %i players: ---\n" % game.no_of_players)

    p = 0

    for player in game.players:                                                 # Connect players to rating list

      file.write("%s %-30s %5i\n" % (player.type, player.name, game.final_points[p]))

      if player.rating_list_entry == None:

        if player.name in rating_list:

          player.rating_list_entry = rating_list[player.name]

        else:

          player.rating_list_entry = add_player(player, rating_list)                    # Register a new player, if necessary

      p += 1

    evaluate_game(game)                                                         # Evaluate game

  file.close()

  save_rating_list_to_file(rating_list)


def evaluate_games_from_file(game_results_file):

  games = []

  file = open(game_results_file, "r")

  content = file.readlines()

  line_index = 0

  while (line_index < len(content)):

    game = Cgame()

    no_of_players = int(content[line_index].split()[3])

    game.no_of_players = no_of_players
    game.players = []
    game.final_points = []

    for p in range(no_of_players):

      line_index += 1

      line = content[line_index].split()

      type =       line[0]
      name =       line[1]
      points = int(line[2])

      player = Cplayer(type, name, None, None, None, None)

      game.players.append(player)
      game.final_points.append(points)

    games.append(game)

    line_index += 1

  file.close()

  evaluate_list(games)


def permutations(no_of_players):

  if no_of_players == 1:
    return [[1]]

  lower_permutations = permutations(no_of_players - 1)

  permutations_list = []

  for lp in lower_permutations:

    for position in range(no_of_players):

      perm = list(lp)

      perm.insert(position, no_of_players)

      permutations_list.append(perm)

  return permutations_list


def compare_players(desired_no_of_games, no_of_players, players):

  perms = permutations(no_of_players)

  no_of_perms = len(perms)

  no_of_repetitions = int(desired_no_of_games / no_of_perms)

  if no_of_repetitions * no_of_perms != desired_no_of_games:

    if abs((no_of_repetitions + 1) * no_of_perms - desired_no_of_games) < abs(no_of_repetitions * no_of_perms - desired_no_of_games):

      no_of_repetitions += 1

  if no_of_repetitions == 0:
    no_of_repetitions = 1

  games = []

  game_id = 0

  for r in range(no_of_repetitions):
    for perm in perms:

      game_id += 1

      permutated_players = [players[perm[i] - 1] for i in range(no_of_players)]

      games.append(play(game_id, False, no_of_players, permutated_players))

  evaluate_list(games)

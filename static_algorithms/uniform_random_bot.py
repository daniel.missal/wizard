from random import randint



class Cvirtual_game:

  def __init__(self, no_of_players, assigned_player_no):

    self.no_of_players = no_of_players
    self.assigned_player_no = assigned_player_no


def create_virtual_game(no_of_players, assigned_player_no):

  #print("")
  #print("uniform_random_bot: Registering a new game of % s players with the assigned player number %s." % (no_of_players, assigned_player_no))

  virtual_game = Cvirtual_game(no_of_players, assigned_player_no)

  return virtual_game


def choose_trump(virtual_game, no_of_players, round_no, hand):

  #print("")
  #print("uniform_random_bot: Choosing a trump color.")

  trump = randint(1, 4)

  return trump


def choose_bid(virtual_game, no_of_players, round_no, trump, trump_card, bid_sequence, hand, players, all_cards):

  #print("")
  #print("uniform_random_bot: Choosing a bid.")

  # The return value of the function must be an integer of the set {0, 1, ... , round_no} and incase the bid_sequence has
  # (no_of_players - 1) entries, it must not be (round_no - "the sum of all entries in bid_sequence").

  if len(bid_sequence) == (virtual_game.no_of_players - 1):

    sum_of_bids = sum(bid_sequence)

    bid = round_no - sum_of_bids

    while bid == (round_no - sum_of_bids):

      bid = randint(0, round_no)

  else:

    bid = randint(0, round_no)

  return bid


def choose_card_to_play(virtual_game, no_of_players,
                        round_no, trick_no,
                        trump, trump_card,
                        mc_index, bid_sequence, player_sequence, card_sequence,
                        hand, playable_cards,
                        strongest_card_played, leading_color,
                        tricks, trick,
                        trick_starter, player, players,
                        deck_size):

  #print("")
  #print("uniform_random_bot: Choosing a card to play")

  index_of_choice = randint(0, len(playable_cards) - 1)

  return index_of_choice

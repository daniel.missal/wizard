from math   import factorial as fac
from random import randint
from game   import *

from numpy  import array



class Cvirtual_game:

  def __init__(self, no_of_players, assigned_player_no):

    self.no_of_players      = no_of_players
    self.assigned_player_no = assigned_player_no


def create_virtual_game(no_of_players, assigned_player_no):

  virtual_game = Cvirtual_game(no_of_players, assigned_player_no)
  return virtual_game


def choose_trump(virtual_game, no_of_players, round_no, hand):

  #print("")
  #print("tendency_bot: Choosing a trump color.")

  cards_of_color =          [0, 0, 0, 0, 0]                     # White, red, yellow, green, blue
  sum_of_numbers_of_color = [0, 0, 0, 0, 0]                     #
  #mean_number_of_color =    [0, 0, 0, 0, 0]                     #
  rating_of_color =         [0, 0, 0, 0, 0]                     #

  for card in hand:

    color = card.color

    cards_of_color[color] += 1
    sum_of_numbers_of_color[color] += card.number

  for color in range(0, 5):
    #mean_number_of_color[color] = float(sum_of_numbers_of_color[color] / cards_of_color[color])
    rating_of_color[color] = float(cards_of_color[color]**2 + sum_of_numbers_of_color[color])

  highest_rated_color = -1
  highest_rating      = -1

  for color in range(1, 5):

    if rating_of_color[color] > highest_rating:

      highest_rated_color = color
      highest_rating      = rating_of_color[color]

  trump = highest_rated_color

  #display_cards(hand)
  #print("Rating of colors:")
  #print(rating_of_color)
  #print("Choice:")
  #print(trump)
  #input("Press enter to continue!")

  return trump


def choose_bid(virtual_game, no_of_players, round_no, trump, trump_card, bid_sequence, hand, players, all_cards):

  # Keeping track of the cards which could possibly be in enemies' hands:

  cards_left = [[], [], [], [], []]                                     # These lists will hold the cards of the respective color.

  for card in all_cards:
    cards_left[card.color].append(card)

  virtual_game.cards_left   = cards_left
  virtual_game.jesters_left = 4
  virtual_game.wizards_left = 4

  if trump_card != None:

    cards_left[trump_card.color].remove(trump_card)                     # The trump card cannot be in enemies' hands any more and thus it is removed from the corresponding list.

    if trump_card.number == 0:
      virtual_game.jesters_left -= 1

    if trump_card.number == 14:
      virtual_game.wizards_left -= 1

  for card in hand:

    cards_left[card.color].remove(card)                                 # The cards in the player's hand cannot be in enemies' hands and thus are removed from the list.

    if card.number == 0:
      virtual_game.jesters_left -= 1

    if card.number == 14:
      virtual_game.wizards_left -= 1


  # Choosing a bid:

  avg_part = float(round_no / no_of_players)

  bid = 0

  for card in hand:
    if ((card.number >= 10) or ((trump > 0) and (card.color == trump))):
      bid += 1

  bid -= 1

  if bid < 0:
    bid = 0

  if len(bid_sequence) == (virtual_game.no_of_players - 1):

    if bid == (round_no - sum(bid_sequence)):

      #print("Illegal bid = %i" % bid)
      #print("avg_part = %f" % avg_part)

      if avg_part < bid:
        bid -= 1
      else:
        bid += 1

  #display_cards(hand)
  #print("trump = %i" % trump)
  #print("Choice:")
  #print(bid)
  #input("Press enter to continue!")

  return bid


def choose_card_to_play(virtual_game, no_of_players,
                        round_no, trick_no,
                        trump, trump_card,
                        mc_index, bid_sequence, player_sequence, card_sequence,
                        hand, playable_cards,
                        strongest_card_played, leading_color,
                        tricks, trick,
                        trick_starter, player, players,
                        deck_size):

  # Keeping track of the cards which could possibly be in enemies' hands:

  cards_left = virtual_game.cards_left

  if trick_no > 1:

    for card in tricks[trick_no - 2].card_sequence:

      if card in cards_left[card.color]:

        cards_left[card.color].remove(card)                             # This card has been played and thus it cannot be in enemies' hands and is removed from the list.

        if card.number == 0:
          virtual_game.jesters_left -= 1

        if card.number == 14:
          virtual_game.wizards_left -= 1

  for card in card_sequence:

    cards_left[card.color].remove(card)                                 # This card has been played and thus it cannot be in enemies' hands and is removed from the list.

    if card.number == 0:
      virtual_game.jesters_left -= 1

    if card.number == 14:
      virtual_game.wizards_left -= 1


  # Choosing a card to play:

  scalarX = player.scalarX
  model   = player.model

  best_index  = 0
  best_number = -100000000

  index = 0

  for card in playable_cards:

    observation = gather_data(card,
                              no_of_players,
                              round_no, trick_no,
                              trump, trump_card,
                              mc_index, bid_sequence, player_sequence, card_sequence,
                              hand, playable_cards,
                              strongest_card_played, leading_color,
                              tricks, trick,
                              trick_starter, player, players,
                              deck_size,
                              cards_left, virtual_game.jesters_left, virtual_game.wizards_left)

    Xnew = array([observation])
    Xnew = scalarX.transform(Xnew)
    
    Ynew = model.predict(Xnew)

    if Ynew > best_number:

        best_index = index
        best_number = Ynew

    index += 1

  return best_index

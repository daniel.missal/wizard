class Cround:

  def __init__(self):

    self.mc            = None
    self.round_starter = None
    self.hands         = None
    self.trump_card    = None
    self.trump_color   = None
    self.bids          = None
    self.tricks        = None
    self.won_tricks    = None
    self.gained_points = None


# The class Ctrick is declared in game.py because only code that is used only for learning data is supposed to be contained here in data.py,
# but the class Ctrick is also used in game.py to store information for static playing algorithms.
# From Ctrick-objects information about already played cards can be obtained and is passed to static playing algorithms in game.py.
#
# While it is possible to reduce redundant data in Cgame and Cround (some information are deducible from other information),
# these methods are not applied because performance is given the highest priority here.


def is_new_strongest_card(trump, leading_color, strongest_card_played, card_played):    # Return True iff the card just played is the strongest card of the trick yet

  if strongest_card_played == -1:                         # Has no card been played yet?
    return True

  elif strongest_card_played.number == 0:                 # Is the strongest card played a jester?

    if card_played.number > 0:
      return True

  elif strongest_card_played.number != 14:                # Is the strongest card played a non-white card? (Conclusion)

    if card_played.number == 14:                                  # Is the card played a wizard?

      return True

    elif trump == 0:                                              # Is there no trump and the card played is not a wizard? (Conclusion)
                                                                  # In this case, the strongest card played must be of leading color

      if ((card_played.color == strongest_card_played.color) and (card_played.number > strongest_card_played.number)):
        return True

    else:                                                         # If there is a trump and the card played is not a wizard... (Conclusion)

      if card_played.color == strongest_card_played.color:

        if card_played.number > strongest_card_played.number:
          return True

      elif strongest_card_played.color != trump:

        if card_played.color == trump:
          return True

  return False


def gather_data(played_card,
                no_of_players,
                round_no, trick_no,
                trump, trump_card,
                mc_index, bid_sequence, player_sequence, card_sequence,
                hand, playable_cards,
                strongest_card_played, leading_color,
                tricks, trick,
                trick_starter, player, players,
                deck_size,
                cards_left, jesters_left, wizards_left):
  
  played_card_color  = played_card.color
  played_card_number = played_card.number
  
  
  # Initialization:
  
  #player_index         - Does not neet initialization
  played_card_no        = [0, 0, 0, 0, 0]
  # new_strongest_card  - Does not neet initialization
  stronger_cards        = [0, 0, 0, 0, 0]  # Stores how many cards of each color would be stronger than played_card after played_card is played in the current position.
  # players_left        - Does not neet initialization          # Stores how many players still have to play a card this trick after played_card is played.
  # tricks_left         - Does not neet initialization
  # trump               - Does not neet initialization
  jesters_in_hand       = 0
  wizards_in_hand       = 0
  cards_of_color        = [0, 0, 0, 0, 0]
  strength_of_color     = [0, 0, 0, 0, 0]
  cards_of_trump        = 0
  strength_of_trump     = 0
  potential             = 0
  # wizards_left        - Does not neet initialization
  # card-sequence       - Does not neet initialization
  # player-bids         - Does not neet initialization
  # player-tricks       - Does not neet initialization
  
  
  # Evaluation of features:
  
  player_index = players.index(player)
  played_card_no[played_card_color] = played_card_number
  
  new_strongest_card = int(is_new_strongest_card(trump, leading_color, strongest_card_played, played_card))
  
  if played_card_number < 14:                   # Not a wizard
    
    stronger_cards[0] += wizards_left
    
    if played_card_number == 0:                           # Jester
      
      for color in range(1, 5):
        stronger_cards[color] = len(cards_left[color])
    
    elif leading_color <= 0:                              #    1) No leading color has been set yet, so the played_card will set the leading color.
                                                          # or 2) A wizard has been played as the first non-jester card and therefore has set the leading color to 0.
      
      for card in cards_left[played_card_color]:
        if card.number > played_card_number:
          stronger_cards[played_card_color] += 1
      
      if ((trump > 0) and (trump != played_card_color)):
        for card in cards_left[trump]:
          stronger_cards[trump] += 1
    
    elif leading_color > 0:                              # A wizard has been played as first non-jester card and therefore has set the leading color to 0.
      
      for card in cards_left[played_card_color]:
        if card.number > played_card_number:
          stronger_cards[played_card_color] += 1
          
      if (leading_color != played_card_color):
        for card in cards_left[leading_color]:
          stronger_cards[leading_color] += 1
      
      if ((trump > 0) and (trump != played_card_color) and (trump != leading_color)):
        for card in cards_left[trump]:
          stronger_cards[trump] += 1
  
  players_left = no_of_players - len(card_sequence) - 1
  tricks_left  = round_no - trick_no + 1
  
  for card in hand:
    
    if   card.number ==  0:
      jesters_in_hand +=  1
      
    elif card.number == 14:
      wizards_in_hand += 14
    
    if ((card.number >= 10) or (card.color == trump)):
      potential += 1
    
    cards_of_color[card.color]    += 1
    strength_of_color[card.color] += card.number**2
    
    if card.color == trump:
      cards_of_trump    += 1
      strength_of_trump += card.number                          # Note: We do NOT take the square of the card.number here.
  
  
  # Final set up of the feature-vector:
  
  observation = [player_index,          # x_01
                 played_card_no[0],     # x_02
                 played_card_no[1],     # x_03
                 played_card_no[2],     # x_04
                 played_card_no[3],     # x_05
                 played_card_no[4],     # x_06
                 new_strongest_card,    # x_07
                 stronger_cards[0],     # x_08
                 stronger_cards[1],     # x_09
                 stronger_cards[2],     # x_10
                 stronger_cards[3],     # x_11
                 stronger_cards[4],     # x_12
                 players_left,          # x_13
                 tricks_left,           # x_14
                 trump,                 # x_15
                 jesters_in_hand,       # x_16
                 wizards_in_hand,       # x_17
                 cards_of_color[1],     # x_18
                 cards_of_color[2],     # x_19
                 cards_of_color[3],     # x_20
                 cards_of_color[4],     # x_21
                 strength_of_color[1],  # x_22
                 strength_of_color[2],  # x_23
                 strength_of_color[3],  # x_24
                 strength_of_color[4],  # x_25
                 cards_of_trump,        # x_26
                 strength_of_trump,     # x_27
                 potential,             # x_28
                 wizards_left]          # x_29
  
  for card in card_sequence:                                            # x_29 ...
    observation.append(card.color)
    observation.append(card.number)
  
  for i in range(2 * (no_of_players - len(card_sequence))):
    observation.append(0)
  
  for p in players:
    observation.append(p.bid)
  
  for p in players:
    observation.append(p.tricks)                                        # ... x_(29 + 4 * no_of_players)
  
  return observation


def display_data(game):

  print("")
  print("")
  print("Game Data:")
  print("==========")
  print("")

  print("id: %s" % game.id)

  no_of_players = game.no_of_players

  print("no_of_players: %s" % no_of_players)

  for player in game.players:

    print("Player: %s" % player.name)

  for game_round in game.rounds:

    print(" mc: %s" % game_round.mc)
    print(" round_starter: %s" % game_round.round_starter)

    for hand in game_round.hands:

      for card in hand:

        print(" Card color: %s" % card.color)
        print(" Card number: %s" % card.number)

    trump_card = game_round.trump_card

    if trump_card != None:
      print(" Trump card color: %s" % trump_card.color)
      print(" Trump card number: %s" % trump_card.number)
    else:
      print(" Trump card color: -1")
      print(" Trump card number: -1")

    print(" trump_color: %s" % game_round.trump_color)

    for bid in game_round.bids:
      print(" Bid: %s" % bid)

    for trick in game_round.tricks:

      for index in trick.player_sequence:
        print("  Player index: %s" % index)

      for card in trick.card_sequence:
        print("  Card color: %s" % card.color)
        print("  Card number: %s" % card.number)

      print("  Trick winner index: %s" % trick.winner)

    for won_tricks in game_round.won_tricks:
      print(" Won tricks: %s" % won_tricks)

    for gain in game_round.gained_points:
      print(" Gained points: %s" % gain)

  for points in game.final_points:
    print("Final points: %s" % points)


def write_data(games, learning_output_file):

  print("")
  print("")
  print("Writing learning data into file '%s'." % learning_output_file)

  file = open(learning_output_file, "w")

  for game in games:
    
    no_of_players = game.no_of_players
    
    observation_index = 0
    
    for game_round in game.rounds:
      
      for trick in game_round.tricks:
        
        index = 0
        
        for card in trick.card_sequence:
          
          player_index = trick.player_sequence[index]
          
          line_elements = game.data[observation_index]
          
          for element in line_elements:
            
            file.write(str(element) + ",")
          
          file.write(str(game_round.gained_points[player_index]) + "\n")
          
          index             += 1
          observation_index += 1
  
  file.close()


def write_data_old(games, learning_output_file):

  print("")
  print("")
  print("Writing learning data into file '%s'." % learning_output_file)

  file = open(learning_output_file, "w")

  for game in games:

    #print("New game:")
    file.write("New game:\n")

    #print("id: %s" % game.id)
    file.write(str(game.id) + "\n")

    no_of_players = game.no_of_players

    #print("no_of_players: %s" % no_of_players)
    file.write(str(no_of_players) + "\n")

    for player in game.players:

      #print("Player: %s" % player.name)
      file.write(player.name + "\n")

    for game_round in game.rounds:

      #print(" mc: %s" % game_round.mc)
      file.write(str(game_round.mc) + "\n")

      #print(" round_starter: %s" % game_round.round_starter)
      file.write(str(game_round.round_starter) + "\n")

      for hand in game_round.hands:

        for card in hand:

          #print(" Card color: %s" % card.color)
          file.write(str(card.color) + "\n")

          #print(" Card number: %s" % card.number)
          file.write(str(card.number) + "\n")

      trump_card = game_round.trump_card

      if trump_card != None:

        #print(" Trump card color: %s" % trump_card.color)
        file.write(str(trump_card.color) + "\n")

        #print(" Trump card number: %s" % trump_card.number)
        file.write(str(trump_card.number) + "\n")

      else:

        #print(" Trump card color: -1")
        file.write("-1\n")

        #print(" Trump card number: -1")
        file.write("-1\n")

      #print(" trump_color: %s" % game_round.trump_color)
      file.write(str(game_round.trump_color) + "\n")

      for bid in game_round.bids:

        #print(" Bid: %s" % bid)
        file.write(str(bid) + "\n")

      for trick in game_round.tricks:

        for index in trick.player_sequence:

          #print("  Player index: %s" % index)
          file.write(str(index) + "\n")

        for card in trick.card_sequence:

          #print("  Card color: %s" % card.color)
          file.write(str(card.color) + "\n")

          #print("  Card number: %s" % card.number)
          file.write(str(card.number) + "\n")

        #print("  Trick winner index: %s" % trick.winner)
        file.write(str(trick.winner) + "\n")

      for won_tricks in game_round.won_tricks:

        #print(" Won tricks: %s" % won_tricks)
        file.write(str(won_tricks) + "\n")

      for gain in game_round.gained_points:

        #print(" Gained points: %s" % gain)
        file.write(str(gain) + "\n")

    for points in game.final_points:

      #print("Final points: %s" % points)
      file.write(str(points) + "\n")

    #print("")
    file.write("\n")

  file.close()

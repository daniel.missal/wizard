


# my_learning_agent NN with static bid
#trained tendency_bot_alpha_6 tendency_bot_alpha_8 tendency_bot_alpha_2



Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1695      s    1002          197100           790          -290       196.707
my_learning_agent                1753      l    1002          110470           650          -280       110.250
uniform_random_bot               1052      s    1002         -593770           -80         -1050      -592.585

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1548      s     102           19780           600          -160       193.922
greedy_bot                       1455      s     102            2230           490          -310        21.863
my_learning_agent                1497      l     102           -4150           390          -290       -40.686

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
my_learning_agent                1480      l     102           16270           710          -250       159.510
tendency_bot_alpha_6             1516      s     102           14630           550          -170       143.431
tendency_bot_alpha_8             1504      s     102            8740           530          -250        85.686



# my_new_learning_agent NN with NN bid
#trained tendency_bot_alpha_6 tendency_bot_alpha_8 tendency_bot_alpha_2


Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1677      s     102           17700           650          -150       173.529
my_new_learning_agent            1711      l     102           15980           570          -170       156.667
uniform_random_bot               1112      s     102          -58200          -220          -860      -570.588

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1574      s     102           19190           620          -310       188.137
greedy_bot                       1557      s     102            4270           530          -300        41.863
my_new_learning_agent            1369      l     102           -7080           290          -300       -69.412

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
my_new_learning_agent            1538      l     102           18700           470          -210       183.333
tendency_bot_alpha_8             1495      s     102           11940           400          -290       117.059
tendency_bot_alpha_6             1467      s     102           10150           580          -250        99.510



Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
my_learning_agent                1680      l     102           10880           440          -210       106.667
my_new_learning_agent            1694      l     102            8480           430          -240        83.137
uniform_random_bot               1126      s     102          -55780          -100          -960      -546.863

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
greedy_bot                       1513      s     102            4160           410          -290        40.784
my_learning_agent                1555      l     102            -310           460          -370        -3.039
my_new_learning_agent            1432      l     102           -5030           290          -390       -49.314

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1569      s     102           17950           620          -200       175.980
my_learning_agent                1436      l     102           11660           540          -220       114.314
my_new_learning_agent            1495      l     102            6700           480          -240        65.686



# my_evolved_learning_agent NN evolved
#trained tendency_bot_alpha_8 my_learning_agent my_new_learning_agent



Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1754      s     102           21880           570          -120       214.510
my_evolved_learning_agent        1635      l     102            6510           390          -250        63.824
uniform_random_bot               1111      s     102          -59690            60         -1000      -585.196

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1527      s     102           19460           530          -210       190.784
greedy_bot                       1518      s     102            7730           670          -250        75.784
my_evolved_learning_agent        1455      l     102            1280           610          -290        12.549

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_6             1611      s     102           21120           600           -70       207.059
tendency_bot_alpha_8             1476      s     102           12740           590          -240       124.902
my_evolved_learning_agent        1413      l     102            4170           420          -260        40.882

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1575      s     102           20460           610          -180       200.588
my_new_learning_agent            1514      l     102           13800           450          -260       135.294
my_evolved_learning_agent        1411      l     102            9850           400          -300        96.569

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1619      s     102           19300           660          -260       189.216
my_evolved_learning_agent        1442      l     102            9830           400          -190        96.373
my_learning_agent                1439      l     102            8300           480          -320        81.373

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
my_learning_agent                1775      l     102           17110           640          -130       167.745
my_evolved_learning_agent        1615      l     102           10260           490          -300       100.588
uniform_random_bot               1110      s     102          -61380           -20          -960      -601.765



# my_learning_greedy_agent NN
#trained tendency_bot_alpha_8 trained tendency_bot_alpha_6 greedy_bot



Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1781      s     102           24150           760          -150       236.765
my_learning_greedy_agent         1614      l     102           13610           820          -190       133.431
uniform_random_bot               1105      s     102          -60880          -210          -990      -596.863

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1471      s     102           16320           550          -230       160.000
my_learning_greedy_agent         1560      l     102            9400           420          -210        92.157
greedy_bot                       1469      s     102            5610           490          -350        55.000

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1458      s     102           16110           640          -210       157.941
my_learning_greedy_agent         1537      l     102           15520           500          -190       152.157
tendency_bot_alpha_6             1505      s     102           11020           490          -210       108.039



# softmax_learner relu, softmax, relu
#trained tendency_bot_alpha_8 trained tendency_bot_alpha_6 greedy_bot



Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1797      s     102           23780           670          -140       233.137
softmax_learner                  1597      l     102           11690           520          -220       114.608
uniform_random_bot               1106      s     102          -61260          -110          -990      -600.588

Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
softmax_learner                  1482      l     102           14860           480          -240       145.686
tendency_bot_alpha_8             1513      s     102           13940           540          -240       136.667
tendency_bot_alpha_6             1505      s     102           10330           630          -290       101.275



# multi_learner 3 4
#trained tendency_bot_alpha_8 trained tendency_bot_alpha_6 greedy_bot uniform_random_bot



Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1710      s     102           20620           790          -210       202.157
multi_learner                    1676      l     102           11720           540          -270       114.902
uniform_random_bot               1114      s     102          -58700          -110         -1060      -575.490


Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points
==============================================================================================================
tendency_bot_alpha_8             1508      s     102           16210           630          -200       158.922
multi_learner                    1528      l     102           16070           590          -130       157.549
tendency_bot_alpha_6             1464      s     102           11500           430          -270       112.745


Name                           Rating   Type   Games   Sum of points   Max. points   Min. points   Avg. points  Std. points   Won games
=======================================================================================================================================
tendency_bot_alpha_8             1570      s     102           20590           670          -170       201.863      176.439      68.000
greedy_bot                       1541      s     102            3990           400          -220        39.118      136.217      20.500
multi_learner                    1389      l     102           -1690           420          -330       -16.569      137.908      13.500


Name                           Rating   Type   Games   Won games   Sum of points   Max. points   Min. points  Std. points   Avg. points
=======================================================================================================================================
featured_softmax_bot             1766      l     102      77.500           32840           700            20      151.910       321.961
tendency_bot_alpha_8             1608      s     102      24.500           16060           730          -350      177.801       157.451
uniform_random_bot               1126      s     102       0.000          -61810          -150          -960      162.862      -605.980


NN_5             (50,40,20,  5 epochs, recorded_games =  5000, enemies: ten8, ten6, greedy, random)
Name                           Rating   Type   Games   Won games   Sum of points   Max. points   Min. points  Std. points   Avg. points
=======================================================================================================================================
NN_5_5000                        1830      l     102      79.500           37490           870          -130      173.530       367.549
tendency_bot_alpha_8             1588      s     102      22.500           18730           640          -170      149.219       183.627
uniform_random_bot               1082      s     102       0.000          -59980          -170          -940      166.145      -588.039


NN_6             (70,40,20,10,   epochs = 3, recorded_games =  5k, loss = 0.0088, enemies: ten8, ten6, greedy, random)
Name                           Rating   Type   Games   Won games   Sum of points   Max. points   Min. points  Std. points   Avg. points
=======================================================================================================================================
NN_6_5000                        1701      l     102      60.500           25460           720          -100      180.391       249.608
tendency_bot_alpha_8             1693      s     102      41.500           20380           550          -230      167.168       199.804
uniform_random_bot               1106      s     102       0.000          -58060          -230          -940      149.782      -569.216


NN_6             (70,40,20,10,   epochs = 3, recorded_games = 25k, loss = 0.0075, enemies: ten8, ten6, greedy, random)
Name                           Rating   Type   Games   Won games   Sum of points   Max. points   Min. points  Std. points   Avg. points
=======================================================================================================================================
NN_6_25k                         1813      l     102      82.000           39230           900            30      157.603       384.608
tendency_bot_alpha_8             1598      s     102      20.000           16630           570          -220      165.540       163.039
uniform_random_bot               1089      s     102       0.000          -59770           -90          -970      179.018      -585.980
